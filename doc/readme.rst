Overview
========

.. {# pkglts, glabpkg_dev

.. image:: https://revesansparole.gitlab.io/manparams/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/manparams/1.0.1/


.. image:: https://revesansparole.gitlab.io/manparams/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/manparams


.. image:: https://revesansparole.gitlab.io/manparams/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/manparams/


.. image:: https://badge.fury.io/py/manparams.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/manparams




main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/revesansparole/manparams/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/revesansparole/manparams/commits/main

.. |main_coverage| image:: https://gitlab.com/revesansparole/manparams/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/revesansparole/manparams/commits/main
.. #}

Management of parameters for simulations purposes
