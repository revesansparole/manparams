"""
Define a set of normalized values that can be used in params to set custom values
with specific meaning instead of using None.
"""

unset = "unset"
"""Flag value for params that need post-computation
"""
